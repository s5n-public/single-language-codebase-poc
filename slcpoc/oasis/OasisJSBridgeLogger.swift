//
// Created by Manuel Ott on 29.07.22.
//

import Foundation
import OasisJSBridge

class OasisJSBridgeLogger: JSBridgeLoggingProtocol {
    func log(level: JSBridgeLoggingLevel, message: String, file: StaticString, function: StaticString, line: UInt) {
        print("\(level) \(message)")
    }
}