//
// Created by Manuel Ott on 28.07.22.
//

import Foundation
import Starscream

/// Swift native websocket client
/// note: using Starscream as websocket client library as it seemed easier to use
class SlcPocWebSocketClient: WebSocketDelegate {

    private var socket: WebSocket
    private var isConnected = false

    init(_ url: String) {
        var request = URLRequest(url: URL(string: url)!)
        request.timeoutInterval = 5
        socket = WebSocket(request: request)
        socket.delegate = self
        socket.connect()
    }

    func send(_ message: String) -> Void {
        socket.write(string: message)
    }

    func close() -> Void {
        print("Closing connection")
        socket.disconnect()
    }

    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            isConnected = true
            print("websocket is connected: \(headers)")
        case .disconnected(let reason, let code):
            isConnected = false
            print("websocket is disconnected: \(reason) with code: \(code)")
        case .text(let string):
            print("Received text: \(string)")
        case .binary(let data):
            print("Received data: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            isConnected = false
        case .error(let error):
            isConnected = false
            handleError(error)
        }
    }


    private func handleError(_ error: Error?) {
        print(error)
    }
}