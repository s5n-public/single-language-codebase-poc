//
// Created by Manuel Ott on 25.07.22.
//

import Foundation
import JavaScriptCore

/// Allows for simpler getters and setters:
/// See: https://christiantietze.de/posts/2020/06/javascriptcore-subscript-swift/
/// context["foo"] instead of context.objectForKeyedSubscript("foo")
extension JSContext {
    subscript(_ key: NSString) -> JSValue? {
        get {
            objectForKeyedSubscript(key)
        }
    }

    subscript(_ key: NSString) -> Any? {
        get {
            objectForKeyedSubscript(key)
        }
        set {
            setObject(newValue, forKeyedSubscript: key)
        }
    }
}

extension JSValue {
    subscript(_ key: NSString) -> JSValue? {
        get {
            objectForKeyedSubscript(key)
        }
    }

    subscript(_ key: NSString) -> Any? {
        get {
            return objectForKeyedSubscript(key)
        }
        set {
            setObject(newValue, forKeyedSubscript: key)
        }
    }
}