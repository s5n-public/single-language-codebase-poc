//
//  JavaScriptContextUtil.swift
//  slcpoc
//
//  Created by Manuel Ott on 21.07.22.
//

import Foundation
import JavaScriptCore


struct JSContextUtil {

    static func create(_ vm: JSVirtualMachine) -> JSContext? {
        // context is like "window" object (web browser)
        // it is possible to pass objects between contexts (given that they reside in the same virtual machine)
        let context = JSContext(virtualMachine: vm)!

        // exception handler; otherwise script might fail silently during evaluation
        context.exceptionHandler = { context, exception in
            print("Unexpected error in javascript context: \(exception!).")
        }

        // add simple console.log handler
        let logHandler: @convention(block) (String) -> Void = { string in
            print("js context log: \(string)")
        }
        context["console"]?["log"] = logHandler

        return context

    }

    static func loadScript(_ scriptName: String, context: JSContext?) -> Void {
        guard
                let scriptUrl = Bundle.main.url(forResource: scriptName, withExtension: "js")
        else {
            print("Unable to read resource files.")
            return
        }

        do {
            let script = try String(contentsOf: scriptUrl, encoding: String.Encoding.utf8)
            context?.evaluateScript(script, withSourceURL: scriptUrl)
        } catch (let error) {
            print("Error while processing script file: \(error)")
        }
    }

}
