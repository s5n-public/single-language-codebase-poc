//
//  ContentView.swift
//  slcpoc
//
//  Created by Manuel Ott on 20.07.22.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        ScrollView {
            SamplesView()
            EncryptionView()
            PersistenceView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .preferredColorScheme(.dark)
        ContentView()
            .preferredColorScheme(.light)
    }
}
