//
//  slcpocApp.swift
//  slcpoc
//
//  Created by Manuel Ott on 20.07.22.
//

import SwiftUI

@main
struct slcpocApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

enum Constants {
    static let apiBasePath = "http://127.0.0.1:8083"
    static let webSocketUrl = "ws://localhost:8082"
}
