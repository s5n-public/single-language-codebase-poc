//
//  JavaScriptHelper.swift
//  slcpoc
//
//  Created by Manuel Ott on 21.07.22.
//

import Foundation
import JavaScriptCore

class JavaScriptHelper {

    static let shared = JavaScriptHelper()

        /// building up the javascript context
    lazy var context: JSContext? = {
        let queue = DispatchQueue(label: "js")
        let vm = queue.sync {
            JSVirtualMachine()!
        }

        let context = JSContextUtil.create(vm)!
        
        // register greeter module
        JSContextUtil.loadScript("greeter", context: context)
        
        // register native core features
        SdkCryptEngineJS.registerInContext(context)
        // GreetingApiJS.registerInContext(context)
        SlcPocApiJS.registerInContext(context)
        PersistenceManagerJS.registerInContext(context)

        return context
    }()


    func sayHello(_ encryptedName: Data?) -> String? {
        let helloNameFunction = context?["helloName"]
        return helloNameFunction?.call(withArguments: [encryptedName as Any])?.toString()
    }

    func callBackend(_ encryptedName: Data?) -> Void {
        let function = context?["callBackend"]
        let base64EncodedString = encryptedName?.base64EncodedString()
        print(base64EncodedString)
        function?.call(withArguments: [base64EncodedString])
    }
}
