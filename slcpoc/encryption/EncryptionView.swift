//
//  EncryptionView.swift
//  slcpoc
//
//  Created by Manuel Ott on 20.07.22.
//

import SwiftUI

struct EncryptionView: View {
    let greeting = "Hello POC"

    @State private var name: String = "🦄"

    @ObservedObject var viewModel = EncryptionPoc()

    var body: some View {
        VStack {
            Text(greeting).font(.headline)
            Spacer()
            Group {
                TextField("Name", text: $name)
                        .border(.secondary)
                        .padding()
                Spacer()
                Button(action: { viewModel.encrypt(name) }) {
                    Label("Call greeter.js > helloName", systemImage: "figure.wave")
                }
                Spacer()
            }

            Group {
                Text(viewModel.encryptedNameBase64String)
                Text(viewModel.greetingMessage)
                Button(action: { viewModel.callBackend(name) }) {
                    Label("Call greeter backend", systemImage: "lock.icloud")
                }
                Text(viewModel.apiGreetingMessage)
                Spacer()
            }

            Group {
                Text("WebSocket example (not JavaScriptified)").font(.subheadline)
                Button(action: { viewModel.connectToWsBackend() }) {
                    Label("Connect", systemImage: "link.icloud")
                }
                Button(action: { viewModel.sendToWs(name) }) {
                    Label("Send name", systemImage: "paperplane.circle")
                }
                Button(action: { viewModel.disconnectFromWsBackend() }) {
                    Label("Disconnect", systemImage: "icloud.slash")
                }
            }

        }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
    }

}

struct EncryptionView_Previews: PreviewProvider {
    static var previews: some View {
        EncryptionView()
                .preferredColorScheme(.dark)
        EncryptionView()
                .preferredColorScheme(.light)
    }
}
