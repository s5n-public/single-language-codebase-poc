//
//  EncryptionViewModel.swift
//  slcpoc
//
//  Created by Manuel Ott on 20.07.22.
//

import Foundation
import AppleArchive
import CryptoKit

class EncryptionPoc: ObservableObject {

    let javascriptHelper = JavaScriptHelper()

    var ws : SlcPocWebSocketClient?

    @Published var encryptedNameBase64String = ""
    @Published var greetingMessage = ""
    @Published var apiGreetingMessage = ""
    @Published var websocketMessage = ""


    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateApiGreetingMessage(_:)), name: SlcPocApiJS.notificationName, object: nil)
    }

    func encrypt(_ name: String) -> Void {
        greetingMessage = javascriptHelper.sayHello(encryptName(name)) ?? "something went wrong. check console."
    }


    func callBackend(_ name: String) -> Void {
        javascriptHelper.callBackend(encryptName(name))
    }

    private func encryptName(_ name: String) -> Data? {
        let encryptedName = SdkCryptoEngine.encrypt(name)

        // just debugging output
        encryptedNameBase64String = encryptedName?.base64EncodedString() ?? "something went wrong. check console."
        print(encryptedNameBase64String)

        return encryptedName
    }

    func connectToWsBackend() -> Void {
        ws = SlcPocWebSocketClient(Constants.webSocketUrl)

    }

    func disconnectFromWsBackend() -> Void {
        ws?.close()
    }

    func sendToWs(_ name: String) -> Void {
        ws?.send(name)
    }

    @objc func updateApiGreetingMessage(_ notification: NSNotification?) -> Void {
        apiGreetingMessage = notification?.userInfo?["result"] as? String ?? "something went wrong. check console."
    }
}
