'use strict';

var helloName = function (data) {
    console.log(data);

    // decrypt name using platform native code
    var decryptedName = SdkCryptoEngine.decryptWithEncrypted(data);
    var greeting = "Hello " + decryptedName;

    if (decryptedName) {
        // log the result
        console.log(greeting);
        return greeting;
    } else {
        console.log("Decryption failed")
    }
}

var callBackend = function (encryptedName) {
    var api = SlcPocApi.createWithCallbackName("apiCallback"); // I have not been able to pass a javascript function directly to swift using function types, hence using just the name within this example
    console.log(`base64-encoded encrypted name: ${encryptedName}`)
    api.callApi(encryptedName);
    console.log("this will not wait for callApi")
}

// the native code will use the js context within the completion handler and invoke this given method
var apiCallback = function(response, error) {
    if (error) {
        console.log(error);
        return;
    }

    // console.log(response);
    // console.log(response.name);
    // console.log(response.message);
    
    PersistenceManager.store(response);
    // let responses = PersistenceManager.fetch();
    // console.log(responses);
    // for (const response of responses) {
    //     console.log(response);
    //     console.log(response.name);
    //     console.log(response.message);
    // }

    var decryptedName = SdkCryptoEngine.decryptWithBase64Encoded(response.name);
    var greeting = response.message + " " + decryptedName;

    if (decryptedName) {
        // log the result
        console.log(greeting);
        return greeting;
    } else {
        console.log("Decryption failed")
    }
    return undefined;
}


