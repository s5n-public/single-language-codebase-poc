//
//  HttpPoc.swift
//  slcpoc
//
//  Created by Benjamin Steimer on 21.07.22.
//

import Foundation
import Alamofire
import JavaScriptCore

class GreetingApi {

    let url = Constants.apiBasePath + "/hello"

    func hello(name: String, completionHandler: @escaping (Result<GreetingResponseDto, Error>) -> Void) -> Void {

        let parameters = ["name": name]
        AF.request(url, parameters: parameters).responseData { response in
            switch response.result {
            case .failure(let error):
                completionHandler(Result.failure(error))
            case .success(let data):
                do {
                    let greeting = try JSONDecoder().decode(GreetingResponseDto.self, from: data)
                    completionHandler(Result.success(greeting))
                } catch let error {
                    completionHandler(Result.failure(error))
                }
            }
        }
    }

    // don't know how to use async await plus javascriptcore
    func callHello(name: String) async -> GreetingResponseDto? {
        let parameters = ["name": name]
        let dataTask = AF.request(url, parameters: parameters).serializingDecodable(GreetingResponseDto.self)
        do {
            let result = try await dataTask.value
            return result
        } catch let error {
            print(error)
            return nil
        }
    }

}

/// JavaScript interface of the Greeting API
@objc protocol GreetingApiJSExports: JSExport {

    var callbackName: String { get set }

    func callApi() -> Void

    static func create(callbackName: String) -> GreetingApiJS
}

/// implementation of above interface making use of GreetingApi
@objc class GreetingApiJS: NSObject, GreetingApiJSExports {

    static var context: JSContext?

    var callbackName: String

    init(_ callbackName: String) {
        self.callbackName = callbackName
    }

    class func create(callbackName: String) -> GreetingApiJS {
        GreetingApiJS(callbackName)
    }

    static func registerInContext(_ context: JSContext) {
        context.setObject(GreetingApiJS.self, forKeyedSubscript: "GreetingApi" as NSString)
        self.context = context // keep a ref to use it in completion handler
    }

    func callApi() -> Void {
        GreetingApi().hello(name: "John", completionHandler: completion)
    }

    private func completion(result: Result<GreetingResponseDto, Error>) -> Void {
        switch result {
        case .success(let greeting):
            GreetingApiJS.context![callbackName as NSString]!.call(withArguments: [greeting])
        case .failure(let error):
            GreetingApiJS.context![callbackName as NSString]?.call(withArguments: [nil, error])
        }
    }
}
