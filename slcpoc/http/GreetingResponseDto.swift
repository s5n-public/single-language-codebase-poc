//
//  GreetingResponse.swift
//  slcpoc
//
//  Created by Manuel Ott on 22.07.22.
//

import Foundation
import JavaScriptCore

@objc protocol GreetingResponseDtoJSExports: JSExport {
    var message: String { get set }
    var name: String { get set }
}

@objc class GreetingResponseDto: NSObject, GreetingResponseDtoJSExports, Codable {
    var message: String
    var name: String
    
    init(name: String, message: String) {
        self.name = name
        self.message = message
    }
}
