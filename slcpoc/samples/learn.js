'use strict';

// takes a name and returns a greeting
var sayHello = function (name) {
    return "Hello " + name + " from simple hello function";
};

// this is just a placeholder which will be replaced by a native implementation during runtime
var sayHelloSwift = function () {
    // empty
}

// example to call a native code block
var sayHelloUsingNative = function (name) {
    return sayHelloSwift(name)
}


// example to make use of a whole class
var sayHelloUsingJsExport = function (name) {
    let greeting = Greeting.createWithNameGreeting(name, "Hello");
    console.log(greeting.name)
    console.log(greeting.greeting)
    return greeting.greetingMessage;
}


var divide = function(a, b) {
    divideUsingPromise(a, b)
        .then(r => console.log(r))
        .catch(err => console.log(err));
}

function divideUsingPromise(a, b) {
    return Promise.resolve()
        .then(() => {
            if (b === 0) throw new Error("b is zero!");
            return a / b;
        });
}

// not working as expected when called from swift. fields will be undefined...
var polygon = class Polygon {
    width = 1;
    height = 1;

    constructor(height, width) {
        console.log("constructor called");
        this.height = height;
        this.width = width;
    }

    get area() {
        console.log("area called");
        return this.calcArea();
    }

    calcArea() {
        console.log("calcArea called using height: " + this.height + " width: " + this.width);
        return this.height * this.width;
    }

    static create(height, width) {
        console.log("factory method called with height: " + height + " width: " + width);
        var poly = new Polygon(height, width);
        console.log(poly.area);
        return poly;
    }
};