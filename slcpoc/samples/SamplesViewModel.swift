//
//  EncryptionViewModel.swift
//  slcpoc
//
//  Created by Manuel Ott on 20.07.22.
//

import Foundation
import AppleArchive
import CryptoKit

class SamplesViewModel: ObservableObject {
    let helper = JavaScriptCoreSamples.shared

    @Published var greetingMessage1 = ""
    @Published var greetingMessage2 = ""
    @Published var greetingMessage3 = ""

    func triggerExampleCode(_ name: String) -> Void {
        greetingMessage1 = helper.sayHelloSimple(name) ?? "🥺simple example didn't work"
        greetingMessage2 = helper.sayHelloSwift(name) ?? "🥺swift example didn't work"
        greetingMessage3 = helper.sayHelloUsingJSExport(name) ?? "🥺swift example didn't work"
    }

    func sendRequest(_ name: String) -> Void {
        helper.callAsyncHello(name)
    }

    func connectToWsBackend() -> Void {
        helper.connectToWebSocket(Constants.webSocketUrl)
    }

    func disconnectFromWs() -> Void {
        helper.closeWebSocketConnection()
    }

    func sendToWs(_ name: String) -> Void {
        helper.sendToWebSocket(name)
    }


}
