//
//  AsyncSwiftBridge.swift
//  slcpoc
//
//  Created by Benjamin Steimer on 28.07.22.
//

import Foundation
import slcpocapi
import JavaScriptCore

@objc protocol AsyncSwiftBridgeJSExports: JSExport {
    static func callAsyncFunction(_ uuid: String, _ selector: String, _ data: Any) -> Void
}

@objc class AsyncSwiftBridge: NSObject, AsyncSwiftBridgeJSExports {

    static var context: JSContext?

    static func registerInContext(_ context: JSContext) {
        context.setObject(AsyncSwiftBridge.self, forKeyedSubscript: "AsyncSwiftBridge" as NSString)
        self.context = context // keep a ref to use it in completion handler
    }

    /// TODO "reflection"/interface https://stackoverflow.com/questions/61718880/reflection-calling-a-method-with-parameters-by-function-name-in-swift
    /// aus der Doku: JavaScript functions don't convert to native blocks/closures unless they already have a backing from a native block/closure. --> d.h. APIs müssen ein native block closure bereitstellen?
    class func callAsyncFunction(_ uuid: String, _ selector: String, _ data: Any) -> Void {
        GreetingApi().hello(name: "Alex") { response in
            switch response {
            case .success(let data):
                context?["asyncBridgeCallback"]?.call(withArguments: [uuid, data])
            case .failure(let error):
                print(error)
            }
        }
    }


}
