//
//  Greeting.swift
//  slcpoc
//
//  Created by Manuel Ott on 21.07.22.
//

import Foundation
import JavaScriptCore

// notes:
// need to bridge from swift to objective-c first (hence @objc everywhere)
// JSExport protocol signals that we want to export this class to JavaScript
// note: only what is defined within the custom GreetingJSExports protocol will be accessible
@objc protocol GreetingJSExports: JSExport {
    var name: String { get set }
    var greeting: String { get set }
    
    var greetingMessage: String { get }
    
    // this will become `createWithNameGreeting` in javascript
    static func createWith(name: String, greeting: String) -> Greeting
    
}

@objc class Greeting: NSObject, GreetingJSExports {
    
    var name: String // dynamic not needed?
    var greeting: String
    
    init(name: String, greeting: String) {
        self.name = name
        self.greeting = greeting
    }
    
    var greetingMessage: String {
        return "\(greeting) \(name), this is via JSExports"
    }
    
    class func createWith(name: String, greeting: String) -> Greeting {
        return Greeting(name: name, greeting: greeting)
    }
}
