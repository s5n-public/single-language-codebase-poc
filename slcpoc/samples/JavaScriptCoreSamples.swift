//
//  JavaScriptHelper.swift
//  slcpoc
//
//  Created by Manuel Ott on 21.07.22.
//

import Foundation
import JavaScriptCore
import OasisJSBridge

class JavaScriptCoreSamples {

    static let shared = JavaScriptCoreSamples()

    init() {
        context = {
            let queue = DispatchQueue(label: "js")
            // create a virtual machine that will execute JavaScript code
            // having one VM for several JSContext allows a shared object space (which we might need 🤔).
            // btw: having several VMs will allow concurrent execution.
            let vm = queue.sync {
                JSVirtualMachine()!
            }

            let context = JSContextUtil.create(vm)!

            // register some sample script files
            JSContextUtil.loadScript("learn", context: context)
            JSContextUtil.loadScript("asyncBridge", context: context)
            AsyncSwiftBridge.registerInContext(context)
            return context
        }()

        interpreter = {
            JSBridgeConfiguration.add(logger: OasisJSBridgeLogger())
            let jsi = JavascriptInterpreter()
            let oasisJsContext = jsi.jsContext
            JSContextUtil.loadScript("jsc-polyfills-test", context: oasisJsContext)
            return jsi
        }()
    }

    /// building up the javascript context
    var context: JSContext?

    /// building javascript context using OasisJSBridge
    let interpreter: JavascriptInterpreter

    /// simple example to demonstrate how it is possible to call a given javascript function
    func sayHelloSimple(_ name: String) -> String? {
        // that's how you get stuff out of the context (in this example it is a function)
//        let helloFunction = context?.objectForKeyedSubscript("sayHello")
        let helloFunction = context?["sayHello"]
        // call the function and return the result
        return helloFunction?.call(withArguments: [name])?.toString()
    }


    /// simple example to demonstrate how javascript code can call native code during execution
    func sayHelloSwift(_ name: String) -> String? {

        // requirement 1: swift closure bridged to an Objective-c block (this will be called from javascript)
        let sayHelloClosure: @convention(block) (String) -> String = { arg in
            return "Hello " + arg + " from Swift closure"
        }

        // requirement 2: let the context know it
        context?.setObject(sayHelloClosure, forKeyedSubscript: "sayHelloSwift" as NSString)

        // same as in simple example. This time the predefined javascript function calls sayHello-closure.
        return context?.objectForKeyedSubscript("sayHelloUsingNative")?.call(withArguments: [name])?.toString()
    }

    /// example for a class using JSExport. see class for details
    func sayHelloUsingJSExport(_ name: String) -> String? {
        context?.setObject(Greeting.self, forKeyedSubscript: "Greeting" as NSString)

        // inline scripting example
        //        context?.evaluateScript(#"""
        //        function test1() {
        //            let greeting = Greeting.createWithNameGreeting("John", "Hello");
        //            return greeting.greetingMessage;
        //        }
        //        """#)
        //        print(context?.objectForKeyedSubscript("test1").call(withArguments: []))

        return context?.objectForKeyedSubscript("sayHelloUsingJsExport")?.call(withArguments: [name])?.toString()
    }

    func callAsyncHello(_ name: String) -> Void {
        interpreter.jsContext!["asyncHello"]!.call(withArguments: [name])
    }

    func connectToWebSocket(_ url: String) -> Void {
        interpreter.jsContext!["createConnection"]!.call(withArguments: [url])
    }

    func closeWebSocketConnection() {
        interpreter.jsContext!["closeConnection"]!.call(withArguments: [])
    }

    func sendToWebSocket(_ name: String) -> Void {
        interpreter.jsContext!["sendViaWs"]?.call(withArguments: [name])
    }

}
