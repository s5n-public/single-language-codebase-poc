//
//  EncryptionView.swift
//  slcpoc
//
//  Created by Manuel Ott on 20.07.22.
//

import SwiftUI

struct SamplesView: View {
    
    @State private var name: String = "🦄"
    
    @ObservedObject var viewModel = SamplesViewModel()
    
    var body: some View {
        VStack {
            Group {
                Text("Say hello")
                TextField("Name", text: $name)
                        .border(.secondary)
                        .padding()
                Spacer()
            }
            Group {
                Text("JavaScriptCore Samples").font(.subheadline)
                Button(action: { viewModel.triggerExampleCode(name) }) {
                    Label("Say hello", systemImage: "play")
                }
                Spacer()
                Text(viewModel.greetingMessage1)
                Text(viewModel.greetingMessage2)
                Text(viewModel.greetingMessage3)
                Spacer()
            }
            Group {
                Text("XmlHttpRequest example").font(.subheadline)
                Button(action: { viewModel.sendRequest(name) }) {
                    Label("Send", systemImage: "play.circle")
                }
                Spacer()
            }
            Group {
                Text("WebSocket example").font(.subheadline)
                Button(action: { viewModel.connectToWsBackend() }) {
                    Label("Connect", systemImage: "link.icloud")
                }
                Button(action: { viewModel.sendToWs(name) }) {
                    Label("Send name", systemImage: "paperplane.circle")
                }
                Button(action: { viewModel.disconnectFromWs() }) {
                    Label("Disconnect", systemImage: "icloud.slash")
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }

}

struct SamplesView_Previews: PreviewProvider {
    static var previews: some View {
        SamplesView()
            .preferredColorScheme(.dark)
        SamplesView()
            .preferredColorScheme(.light)
    }
}
