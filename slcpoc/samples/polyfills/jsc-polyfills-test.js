// javascript core needs some polyfills.

// using XmlHttpRequest polyfill
function xmlHttpRequestPromisified(name) {
    return new Promise(function (resolve, reject) {
        const request = new XMLHttpRequest();
        request.onload = function () {
            if (request.status >= 200 && request.status < 300) {
                resolve({
                    code: request.status,
                    message: request.statusText,
                    data: request.response
                });
            } else {
                reject({
                    code: request.status,
                    message: request.statusText
                });
            }
        };
        request.onerror = function () {
            reject({
                code: 999,
                message: "error accessing service"
            });
        };

        request.open("GET", "http://localhost:8083/hello?name=" + encodeURIComponent(name), true);
        request.send();
    });
}

async function asyncHello(name) {
    console.log('asyncHello');
    try {
        const result = await xmlHttpRequestPromisified(name);
        console.log(`result object is: ${JSON.stringify(result)}`);
        console.log(`api data is: ${result.data}`);
    } catch (error) {
        console.log(`oh noooo: ${error.message}`);
    }
    console.log('asyncHello end');
}


// using WebSocket polyfill
var ws;

function createConnection() {
    if (ws) {
        console.log("already created");
        return ws;
    }
    console.log('creating connection');
    ws = new WebSocket('ws://localhost:8082');
    ws.onopen = function () {
        console.log('open');
        ws.send('swifty hello');
    };

    ws.onmessage = function (e) {
        console.log(e.data);
    };

    ws.onclose = function () {
        console.log('close');
    };

    ws.onerror = function (error) {
        console.log(error)
    }

    return ws;
}

function sendViaWs(name) {
    if (ws) {
        ws.send(name);
    }
}

function closeConnection() {
    if (ws) {
        ws.close(1000);
        ws = undefined;
    }
}
