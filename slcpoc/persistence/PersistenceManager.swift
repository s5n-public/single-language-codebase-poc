//
//  PersistenceHelper.swift
//  slcpoc
//
//  Created by Benjamin Steimer on 28.07.22.
//

import Foundation
import SQLite
import JavaScriptCore
import slcpocapi

struct PersistenceManager {
    static let shared = PersistenceManager()
    
    private var db: Connection?
    private let responses = Table("responses")
    
    init() {
        try! self.db = Connection(.temporary)
        createTable()
    }
    
    private func createTable() {
        try! db?.run(responses.create { t in
            t.column(Expression<Int64>("id"), primaryKey: true)
            t.column(Expression<String>("message"))
            t.column(Expression<String>("name"))
        })
    }
    
    func store(_ response: GreetingResponse) {
        try! db?.run(responses.insert(response))
    }
    
    func fetch() -> [GreetingResponse] {
        let responses: [GreetingResponse] =  try! db!.prepare(responses).map { row in
            return try! row.decode()
        }
        
        return responses;
    }
}

@objc protocol PersistenceManagerJSExports: JSExport {
    /// this method will become "decryptWithEncrypted(data)"
    static func store(_ response: GreetingResponse)
    static func fetch() -> [GreetingResponse]
}

/// implementation of above interface making use of the SkdCryptoEngine
@objc class PersistenceManagerJS: NSObject, PersistenceManagerJSExports {
    class func registerInContext(_ context: JSContext) {
        context.setObject(PersistenceManagerJS.self, forKeyedSubscript: "PersistenceManager" as NSString)
    }
    
    class func store(_ response: GreetingResponse) {
        PersistenceManager.shared.store(response)
    }
    
    class func fetch() -> [GreetingResponse] {
        let responses = PersistenceManager.shared.fetch()
        return responses
    }
}
