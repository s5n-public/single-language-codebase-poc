//
//  PersistencePoc.swift
//  slcpoc
//
//  Created by Benjamin Steimer on 28.07.22.
//

import Foundation
import slcpocapi

class PersistencePoc: ObservableObject {
    @Published var persistedElements = ""

    func store() {
        let response = GreetingResponse()
        response.message = "Hello Manuel"
        response.name = "Alex"
        PersistenceManager.shared.store(response)
    }
    
    func fetch() {
        persistedElements = ""
        let fetched = PersistenceManager.shared.fetch()
        for element in fetched {
            persistedElements += "name: \(element.name! ?? ""), message:\(element.message ?? "")\n"
        }
    }
}

