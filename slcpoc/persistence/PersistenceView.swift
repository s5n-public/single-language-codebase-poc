//
//  PersistenceView.swift
//  slcpoc
//
//  Created by Benjamin Steimer on 28.07.22.
//

import SwiftUI

struct PersistenceView: View {
    @ObservedObject var viewModel = PersistencePoc()
    
    var body: some View {
        VStack {
            Text("Persitence example").font(.subheadline)
            Button(action: { viewModel.store() }) {
                Label("Store", systemImage: "square.and.arrow.up")
            }
            Spacer()
            Button(action: { viewModel.fetch() }) {
                Label("Fetch", systemImage: "square.and.arrow.down")
            }
            Spacer()
            Text(viewModel.persistedElements)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct PersistenceView_Previews: PreviewProvider {
    static var previews: some View {
        PersistenceView()
    }
}
