//
// Created by Manuel Ott on 27.07.22.
//

import Foundation
import slcpocapi
import JavaScriptCore

/// JavaScript interface of the Greeting API
@objc protocol SlcPocApiJSExports: JSExport {

    var callbackName: String { get set }

    func callApi(_ name: String) -> Void

    static func create(callbackName: String) -> SlcPocApiJS
}

/// implementation of above interface making use of GreetingApi
@objc class SlcPocApiJS: NSObject, SlcPocApiJSExports {

    static let notificationName = Notification.Name("slcpocapi.success")

    static var context: JSContext?

    var callbackName: String

    init(_ callbackName: String) {
        self.callbackName = callbackName
    }

    class func create(callbackName: String) -> SlcPocApiJS {
        SlcPocApiJS(callbackName)
    }

    static func registerInContext(_ context: JSContext) {
        context.setObject(SlcPocApiJS.self, forKeyedSubscript: "SlcPocApi" as NSString)
        self.context = context // keep a ref to use it in completion handler
    }

    func callApi(_ name: String) -> Void {
        // this examples uses the generated OpenAPI client to call the backend
        slcpocapiAPI.basePath = Constants.apiBasePath
        HelloAPI.hello(name: name, completion: completion)
    }

    private func completion(data: GreetingResponse?, error: Error?) -> Void {
        var callback = SlcPocApiJS.context?.objectForKeyedSubscript(callbackName)
        if (data != nil) {
            var result = callback?.call(withArguments: [data]).toString()
            // trigger an event to update the UI in our example
            NotificationCenter.default.post(name: SlcPocApiJS.notificationName, object: nil, userInfo: ["result": result])
        } else {
            callback?.call(withArguments: [nil, error])
        }
    }

}
