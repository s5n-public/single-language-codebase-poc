//
// Created by Manuel Ott on 25.07.22.
//

import Foundation
import JavaScriptCore
import CryptoKit

/// This is the "plain swift" crypto engine providing decryption/encryption (using symmetric keys to keep it simple)
class SdkCryptoEngine {

    static let shared = SdkCryptoEngine(SymmetricKey(size: .bits128))

    let randomSymmetricKey: SymmetricKey

    init(_ symmetricKey: SymmetricKey) {
        let key = "EJ2zuYbarw8/jv1Uie3OPA=="
        guard let keyData = Data(base64Encoded: key) else {
            fatalError("whoops, did you change the key? btw, changing key might break some examples.")
        }
        randomSymmetricKey = SymmetricKey(data: keyData)
    }

    class func decrypt(base64Encoded: String?) -> String? {
        guard let base64EncodedData = base64Encoded else {
            print("Nothing to decrypt")
            return nil
        }
        let data = Data(base64Encoded: base64EncodedData)
        return decrypt(name: data)
    }

    class func decrypt(name: Data?) -> String? {
        do {
            guard let encryptedData = name else {
                print("Nothing to decrypt")
                return nil
            }
            let sealedBox = try AES.GCM.SealedBox(combined: encryptedData)
            let decryptedData = try AES.GCM.open(sealedBox, using: shared.randomSymmetricKey)
            let name = String(data: decryptedData, encoding: .utf8)
            return name
        } catch {
            print("Unexpected error during decryption: \(error)")
            return nil
        }
    }

    class func encrypt(_ plainText: String) -> Data? {
        do {
            let encryptedData = try AES.GCM.seal(plainText.data(using: .utf8)!, using: shared.randomSymmetricKey).combined
            return encryptedData
        } catch {
            print("Unexpected error during encryption: \(error)")
            return nil
        }
    }

}

/// JavaScript interface of the CryptoEngine
@objc protocol SdkCryptoEngineJSExports: JSExport {
    /// this method will become "decryptWithEncrypted(data)"
    static func decrypt(encrypted: Data?) -> String?
    static func decrypt(base64Encoded: String?) -> String?
}

/// implementation of above interface making use of the SkdCryptoEngine
@objc class SdkCryptEngineJS: NSObject, SdkCryptoEngineJSExports {

    class func registerInContext(_ context: JSContext) {
        context.setObject(SdkCryptEngineJS.self, forKeyedSubscript: "SdkCryptoEngine" as NSString)
    }

    class func decrypt(encrypted: Data?) -> String? {
        SdkCryptoEngine.decrypt(name: encrypted)
    }

    class func decrypt(base64Encoded: String?) -> String? {
        SdkCryptoEngine.decrypt(base64Encoded: base64Encoded)
    }
}


