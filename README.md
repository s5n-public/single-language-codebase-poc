# SLCPOC (Single Language Codebase POC)

![](Resources/sequence-diagram.png)

## Backend Mock
The app uses a REST API and a WebSocket server in some examples. Both are mocked in a separate project called `slcpoc-backend`

The REST API specification is provided as OpenAPI spec. The specification file can be found under the `Resources` folder. 

### Generate swift client
Using OpenAPI code generation to build a native client:
```bash
openapi-generator-cli generate -g swift5 -i Resources/slcpoc-api.json -o ../slcpoc-api -c Resources/api-config.yml
```

## JavaScriptCore 

- framework to evaluate JavaScript programs from within Swift, Objective-C
- JavaScript engine for WebKit-based browsers and iOS' Safari
- support of EcmaScript specs is based on the runtime. the minimum supported iOS version is currently 13 (since latest 2 versions are supposed to be supported)

### Notes
- don't use `let` if you want to access it from swift
- use `_` to avoid named parameters functions (blaWithThisThat)

### Open
- debugging javascript core? maybe with this https://engineering.salesforce.com/debugging-webkit-in-hybrid-mobile-apps-2eee63a4c922/
- "hot code replacement" https://github.com/johnno1962/InjectionIII 
- using V8 to have same engine on all platforms? (that's what React Native did)
- polyfill for fetch (instead of XmlHttpRequest)?



